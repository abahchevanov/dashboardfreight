// Gulp plugin setup
var gulp         = require('gulp');

// Watches single files
var watch        = require('gulp-watch');

// Compiles SCSS files
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

// Includes the Bourbon Neat libraries
var neat         = require('node-neat').includePaths;

// Concats your JS files
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');

// Live reload
var browserSync = require('browser-sync');
var reload = browserSync.reload;

var nodemon = require('gulp-nodemon');
var notify = require('gulp-notify');

function handleErrors() {
    var args = Array.prototype.slice.call(arguments);
  
    // Send error to notification center with gulp-notify
    notify.onError({
      title: "Compile Error",
      message: "<%= error %>"
    }).apply(this, args);
  
    // Keep gulp from hanging on this task
    this.emit('end');
  }

gulp.task('watch', ['browser-sync'], function () {
    gulp.watch('./lib/scss/**/*.scss', ['sass']);
    gulp.watch('./lib/js/**/*.js', ['browserify']);
    gulp.watch('lib/images/*.{jpg,jpeg,png,gif,svg}', ['images']);

    gulp.watch("*.html").on('change', browserSync.reload);

    var watcher = watchify(browserify({
        // Specify the entry point of your app
        entries: ['./lib/js/app.js'],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    }));

    return watcher.on('update', function() {
        watcher.bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('./public/assets/'))
    })
});   

gulp.task('browserify', function() {
    return browserify('./lib/js/app.js')
        .bundle()
        .on('error', handleErrors)
        //Pass desired output filename to vinyl-source-stream
        .pipe(source('bundle.js'))
        // Start piping stream to tasks!
        .pipe(gulp.dest('./assets/'));
  });
  
  gulp.task('images', function() {
    return gulp.src('./lib/images/**')
      .pipe(changed('./assets/')) // Ignore unchanged files
      .pipe(imagemin()) // Optimize
      .pipe(gulp.dest('./assets/'))
  });

gulp.task('sass', function () {
    gulp.src('./lib/scss/*.{sass,scss}')
      .pipe(sass({includePaths: neat}))
      .on('error', handleErrors)
      .pipe(autoprefixer({ browsers: ['last 2 version'] }))
      .pipe(gulp.dest('./public/assets/'));
  });

gulp.task('browser-sync', ['nodemon'], function() {
    browserSync({
        proxy: "localhost:4300",  // local node app address
        port: 5000,  // use *different* port than above
        notify: true
    });
});
  
gulp.task('nodemon', function (cb) {
    var called = false;
    return nodemon({
        script: 'app.js',
        ignore: [
        'gulpfile.js',
        'node_modules/'
        ]
    })
    .on('start', function () {
        if (!called) {
        called = true;
        cb();
        }
    })
    .on('restart', function () {
        setTimeout(function () {
        reload({ stream: false });
        }, 1000);
    });
});
  
gulp.task('default', ['watch', 'browser-sync'], function () {
    gulp.watch(['public/*.html'], reload);
  });