const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
var time = require('express-timestamp');
// Use Static Files Folder
app.use(express.static('public/assets'))
app.use('/assets', express.static('public/assets'))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(time.init);
app.set('trust proxy', true);

const port = 4300;


app.get('/', function (request, response) {
  console.log('Request: /')
  fs.readFile('public/index.html', function (err, data) {
    response.writeHead(200, { 'Content-Type': 'text/html' });
    response.write(data);
    response.end();
  });
});

app.get('/popup', function (request, response) {
  console.log('Request: /')
  fs.readFile('public/popup.html', function (err, data) {
    response.writeHead(200, { 'Content-Type': 'text/html' });
    response.write(data);
    response.end();
  });
});

var db = require('./public/logistics-api.js');

app.post('/api/products', function (request, response) {
  console.log('update product');
  var requestBody = request.body; 

  db.updateOrder(requestBody, function(result){
    
    var message = {
      response: 'success'
    }
  
    response.end(JSON.stringify(message));
  
  });
});

////////////
app.get('/api/dates/:time', function (request, response) {

  console.log('Request to API date');
  var time = request.params.time;

  var singleDate = time.indexOf('&') < 0 ? true : false;

  if (singleDate) {
    db.getSingleDate(time, function (data) {
      response.end(JSON.stringify(data));
    });
  } else {
    db.getRangeDates(time, function (data) {
      response.end(JSON.stringify(data));
    });
  }
})

app.listen(port, () => console.log('Server started!', port));
