const express = require('express');
const app = express();

var mysql = require('mysql');

var db_details = {
    host: "mysql43.unoeuro.com",
    user: "magnuslassen_dk",
    password: "Atanas0050",
    database: 'magnuslassen_dk_db'
}
function getSingleDate(date, callback) {

    var connection = mysql.createConnection(db_details);

    // search by date
    connection.query('SELECT * FROM `consignor_data` WHERE `Deliverydate` = "10.08.2018" AND Delivered = 0 LIMIT 30', function (err, data, fields) {
        if (err) throw err

        var orderDetails = [];

        data.forEach(element => {
            orderDetails.push(element);
        });
        callback(orderDetails);
        console.log(orderDetails[0].lat, orderDetails[0].Name1Receiver);
    })

    connection.end()

}
function getRangeDates(date, callback) {

    var connection = mysql.createConnection(db_details);

    // search by date
    connection.query('SELECT * FROM `consignor_data` WHERE `Deliverydate` = "10.08.2018" AND Delivered = 0', function (err, data, fields) {
        if (err) throw err

        var orderDetails = [];

        data.forEach(element => {
            orderDetails.push(element);
        });
        callback(orderDetails);
        console.log(orderDetails[0].lat, orderDetails[0].Name1Receiver);
    })
    connection.end()
}


function updateOrder(data, callback) {

    var connection = mysql.createConnection(db_details);

    var query = 'UPDATE `consignor_data` SET Deliverydate = ?, AllocatedRoute = ?, Allocated = ?, MessageDriver = ?, Delivered = ? WHERE Ordernumber= ?';

    let id = data.Ordernumber;
    let date = data.Deliverydate;
    let allocatedRoute = data.AllocatedRoute;
    let message = data.MessageDriver;
    let delivered = data.Delivered;
    let allocated = data.Allocated;

    connection.query(query, [date, allocatedRoute, allocated, message, delivered, id], function (err, result, rows, fields) {
        if (err) throw err
        callback();
    })

    connection.end()

}


module.exports = {
    getSingleDate: getSingleDate,
    getRangeDates: getRangeDates,
    updateOrder: updateOrder
}


