var datePicker = 0;
var productDetails;
// 0 == all
// 1 == allocated
// 2 == not allocated
var filter = 0;

var deliveredFilter = true;

var autorefresh = false;

var truckRoutes = [];

// Gets active form and with proper id and emits an event with the correct one




events.on('updateProductDetails', function (id) {
    var dataId = `[data-id]`;
    var productFormArr = document.querySelectorAll(dataId);
    for (var i = 0; i < productFormArr.length; i++) {
        var productId = productFormArr[i].getAttribute('data-id');
        if (id === Number(productId)) {
            events.emit('collectFormData', {
                data: productFormArr[i],
                id: id
            });
            break;
        }
    }
})

events.on('collectFormData', function (form) {
    console.log(form);

    var DOMdeliveryDate, DOMregion, DOMtruck, DOMmessage, DOMdelivered;

    DOMdeliveryDate = form.data.querySelector('#date').value;
    DOMregion = form.data.querySelector('#region').value;
    DOMtruck = form.data.querySelector('#truck').value;
    DOMmessage = form.data.querySelector('#message').value;
    DOMdelivered = form.data.querySelector('#delivered').checked;

    var formObject = {
        deliveryDate: DOMdeliveryDate,
        region: DOMregion,
        truck: DOMtruck,
        message: DOMmessage,
        delivered: DOMdelivered
    }
    events.emit('formatFormData', {
        data: formObject,
        id: form.id
    });
})

events.on('formatFormData', function (form) {

    var currentProduct = productDetails[form.id];
    var deliveryDateChanged = true;
    var regionChanged = true;
    var truckChanged = true;
    var messageChanged = true;

    if (form.data.deliveryDate == "") {
        deliveryDateChanged = false;
    }
    if (form.data.region == 'Select') {
        regionChanged = false;
    }
    if (form.data.truck == 'Select') {
        truckChanged = false;
    }
    if (form.data.message == "") {
        messageChanged = false;
    }

    if (deliveryDateChanged) {
        var date = form.data.deliveryDate.replace(new RegExp('-', 'g'), '.');
        currentProduct.Deliverydate = date;
    }
    if (regionChanged && truckChanged) {
        var regionLowerCased = form.data.region.toLowerCase();
        var concatTruck = form.data.truck.toLowerCase().replace(' ', '-');
        var allocatedRoute = regionLowerCased + '-' + concatTruck;
        currentProduct.Allocated = 1;
        currentProduct.AllocatedRoute = allocatedRoute;
    }
    if (messageChanged) {
        currentProduct.MessageDriver = form.data.message;
    }
    console.log(form.data.delivered);
    if (form.data.delivered) {
        currentProduct.Delivered = 1;
    } else {
        currentProduct.Delivered = 0;
    }

    productDetails[form.id] = currentProduct;
    events.emit('sendFormData', currentProduct);


})

events.on('sendFormData', function (product) {
    console.log(product);

    $.ajax({
        method: "POST",
        url: '/api/products',
        data: product,
        success: function (result) {
            var parsedResult = JSON.parse(result);
            console.log(parsedResult.response);
            events.emit('renderItems', productDetails);            
        }
    });


})

events.on('trucks', function (truck) {
    var truckNotInRoutes = true;

    for (var i = 0; i < truckRoutes.length; i++) {
        if (truckRoutes[i] === truck) {
            console.log('Truck in routes...');
            truckRoutes.splice(i, 1);
            truckNotInRoutes = false;
            console.log(truckRoutes);
            break;
        }
    }

    if (truckNotInRoutes) {
        truckRoutes.push(truck);
        console.log(truckRoutes)
    }
    filterByCheckboxes();
})

function filterByCheckboxes() {
    clearPins();
    filterByAllocated();
}

var truckCheckBoxes = document.querySelectorAll('.filter-checkbox');

for (var i = 0; i < truckCheckBoxes.length; i++) {
    var routeValue = truckCheckBoxes[i].value;
    var checkBox = truckCheckBoxes[i];
    truckRoutes.push(routeValue);
    checkBox.addEventListener('change', function () {
        events.emit('trucks', this.value)
    })

}

var elementToday = document.getElementById("today");
var elementDatePicker = document.getElementById("datepicker");

var allocatedButton = document.querySelector('#allocatedFilter');
var unallocatedButton = document.querySelector('#unallocatedFilter');
var allButton = document.querySelector('#allFilter');

var deliveredButton = document.querySelector('#deliveredFilter');
var notDeliveredButton = document.querySelector('#notDeliveredFilter');

var visibleRouteSection = document.getElementById('filter-routes');

var formDate, formRegion, formTruck, formMessageToDriver, formDelivered;


allocatedButton.addEventListener('click', function () {
    filterByAllocated();

    $('#filter-routes').show(500);
})
unallocatedButton.addEventListener('click', function () {
    filterByUnallocated();
     
    $('#filter-routes').hide(500);
})
allButton.addEventListener('click', function () {
    filterByAll();

    $('#filter-routes').hide(500);  
})

// today or datepicker
$("#today").click(function () {
    elementToday.setAttribute(
        "style",
        "background-color:#F4F4F4; border:2px solid #A0D5FF;"
    );
    elementDatePicker.setAttribute("style", "background-color:white;");
    datePicker = 1;
});

$("#datepicker").click(function () {
    elementDatePicker.setAttribute(
        "style",
        "background-color:#F4F4F4; border:2px solid #A0D5FF;"
    );
    elementToday.setAttribute("style", "background-color:white;");
    datePicker = 2;
});

events.on('markerClicked', function (e) {
    console.log("markerclicked handler")
    var productId = undefined;
    productId = document.querySelector('.pin');
    console.log(e);
})

function init() {
    searchButtonFunctionality();
    // todayDate();
}

var timerID = setInterval(function() {
    todayDate()
}, 20 * 60 * 1000);

document.addEventListener("DOMContentLoaded", function (event) {
    init();
});



