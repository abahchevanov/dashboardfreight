function setProductDetails(products) {
  productDetails = products;
}

function filterByDelivered(markers) {

  var newMarkers = [];
  for (var i = 0; i < markers.length; i++) {
    var delivered = markers[i].Delivered;
    if (delivered == 0) {
      newMarkers.push(markers[i]);
    } else {
      newMarkers.push(0);
    }
  }
  return newMarkers;
}

function filterByAllocated() {
  filter = 1;
  clearPins();
  var markersToShow = productDetails.slice(0);
  for (var i = 0; i < markersToShow.length; i++) {

    var aR = markersToShow[i].AllocatedRoute;
    var foundRoute = false;
    truckRoutes.forEach(function (route) {
      if (route == aR) {
        foundRoute = true;
      }
    })

    if (!foundRoute) {
      markersToShow[i] = 0;
    }
  }



  console.log(markersToShow);
  if (deliveredFilter) {
    markersToShow = filterByDelivered(markersToShow);
  }
  console.log('////////')

  for (var e = 0; e < markersToShow.length; e++) {
    googleMarkers[e].setMap(googleMap);
  }
  for (var y = 0; y < markersToShow.length; y++) {
    if (markersToShow[y] === 0) {
      googleMarkers[y].setMap(null);
    }
  }




}

function filterByUnallocated() {
  filter = 2;
  clearPins();
  var markersToShow = productDetails.slice(0);
  for (var i = 0; i < markersToShow.length; i++) {
    console.log(markersToShow[i].Allocated);
    if (markersToShow[i].Allocated > 0) {
      console.log('not zero');
      markersToShow[i] = 0;
    }
  }

  console.log(markersToShow);
  if (deliveredFilter) {
    markersToShow = filterByDelivered(markersToShow);
  }

  for (var e = 0; e < markersToShow.length; e++) {
    googleMarkers[e].setMap(googleMap);
  }
  for (var y = 0; y < markersToShow.length; y++) {
    if (markersToShow[y] === 0) {
      googleMarkers[y].setMap(null);
    }
  }

  // for (var y = 0; y < markersToShow.length; y++) {
  //   if (markersToShow[y].Allocated == 0) {
  //     googleMarkers[y].setMap(googleMap);
  //   }
  // }
}

function filterByAll() {
  filter = 0;
  clearPins();
  var markersToShow = productDetails.slice(0);
  if (deliveredFilter) {
    markersToShow = filterByDelivered(markersToShow);
    console.log(markersToShow);
  }

  for (var e = 0; e < markersToShow.length; e++) {
    googleMarkers[e].setMap(googleMap);
  }
  for (var y = 0; y < markersToShow.length; y++) {
    if (markersToShow[y] === 0) {
      googleMarkers[y].setMap(null);
    }
  }


}

function renderData(data) {
  events.emit("renderItems", data);
}

function clearPins() {
  googleMarkers.forEach(function (marker) {
    marker.setMap(null);
  });
}

function todayDate() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();
  today = mm + "." + dd + "." + yyyy;
  fetchDates(today);
}

function rangeDates() {
  var from = document.querySelector("#datepicker");

  fetchDates(from);
}

function fetchDates(date) {
  var dateToFetch = `/api/dates/${date}`;
  $.ajax({
    method: "GET",
    url: dateToFetch,
    success: function (result) {
      var parsedResult = JSON.parse(result);
      setProductDetails(parsedResult);
      renderData(parsedResult);
    }
  });
}

function searchButtonFunctionality() {
  var searchButton = document.querySelector("#searchDates");
  searchButton.addEventListener("click", function () {
    if (datePicker === 0) {
      console.error("Nothing selected");
    } else if (datePicker === 1) {
      todayDate();
    } else if (datePicker === 2) {
      var rangeDatesStr = document.getElementById("datepicker").value;
      var rangeDatesArr = rangeDatesStr.split(" - ");
      console.log(rangeDatesArr);
      console.log("maika ti, mai kati, maik a ti?");
      // Get date
      fetchDates(date);
    }
  });
}

