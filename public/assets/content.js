function renderContent(product, count){
    
    var orderNumber = product.Ordernumber;
    var name = product.Name1Receiver;
    var deliveryDate = product.Deliverydate; 
    var address = product.Adress1Receiver;
    var postalCode = product.PostalcodeReceiver;
    var countryCode = product.CountrycodeReceiver;
    var contactNumber = product.PhoneReceiver;
    var messageToDriver = product.MessageDriver;
    var city = product.CityReceiver;
    var routeDriver = product.AllocatedRoute;

    console.log(product.Delivered);

    var contentString = `<div class="container popup-content" data-id=${count}>
          <div class="row justify-content-md-center">
              <div class="col-6">
                  <h4>Assign to route:</h4>
                  <label for="datepicker">Edit delivery date:</label>
                  <input id="date" type="date"/>                    
                  <br><br>  
                  <label for="region">Select region:</label>
                  <select id="region">
                      <option>Select</option>
                      <option>Aarhus</option>
                      <option>Ringkøbing</option>
                      <option>Ulfborg</option>
                      <option>Videbæk</option>
                      <option>Viborg</option>
                      <option>Varde</option>
                  </select>  
                  <br><br>  
                  <label for="truck">Select truck:</label>
                  <select id="truck">
                      <option>Select</option>
                      <option>Truck 1</option>
                      <option>Truck 2</option>
                      <option>Truck 3</option>
                      <option>Truck 4</option>
                  </select>  
                  <br><br>  
                  <label for="message">Update message to driver</label>
                  <br>
                  <textarea id="message" rows="5"></textarea>
                  <br><br>                  
                  <br>
                  <label for="delivered">Delivered:</label>
                  <input class="formDelivered" type="checkbox" id="delivered" name="delivered" value="0">                  
              </div>
              <div class="col-6">
                  <h4>Client details:</h4>
                  <p class="details">Order number:</p>
                  <br>
                  <p>${orderNumber}</p>
                  <br>
                  <p class="details">Current delivery date:</p>
                  <br>
                  <p>${deliveryDate}</p>
                  <br>
                  <p class="details">Current route:</p>
                  <br>
                  <p>${routeDriver}</p>
                  <br>
                  <p class="details">Name:</p>
                  <br>
                  <p>${name}</p>
                  <br>
                  <p class="details">Contact number:</p>
                  <br>
                  <p>${contactNumber}</p>
                  <br>
                  <p class="details">Address:</p>
                  <br>
                  <p>${address}</p>
                  <br>
                  <p>${city}</p>
                  <br>
                  <p>${postalCode}</p>
                  <br>
                  <p>${countryCode}</p>
                  <br>
                  <p class="details">Message to Driver:</p>
                  <br>
                  <p>${messageToDriver}</p>
                  <br>
                  <br>
              </div>
          </div>
  
          <div class="row justify-content-md-center">
              <button onclick="emitId(${count})">Save</button>
          </div>
  
      </div>`;


      return contentString;
}