var googleMarkers = [];
var googleMap;

function emitId(id) {
  events.emit('updateProductDetails', id);
}

function initMap() {
  var map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 55.768545, lng: 9.272697 },
    zoom: 8,
    disableDefaultUI: true,
    zoomControl: true
  });

  googleMap = map;

  var layer = new google.maps.FusionTablesLayer({
    query: {
      from: "1CSLRxqx006SKjMyy5FMSUQyqmwpZHKYlteFxFryZ"
    },
    styles: [
      {
        polygonOptions: {
          strokeColor: "#FFFFFF",
          strokeWeight: 2,

          fillColor: "#02905D",
          fillOpacity: 0.3
        }
      },
      {
        // Viborg
        where: "color = 6",
        polygonOptions: {
          fillColor: "#00848F",
          fillOpacity: 0.3
        }
      },
      {
        // Videbæk
        where: "color = 1",
        polygonOptions: {
          fillColor: "#0076B7",
          fillOpacity: 0.3
        }
      },
      {
        // Varde
        where: "color = 2",
        polygonOptions: {
          fillColor: "#9C9F46",
          fillOpacity: 0.3
        }
      },
      {
        // Aarhus
        where: "color = 3",
        polygonOptions: {
          fillColor: "#B61A48",
          fillOpacity: 0.3
        }
      },
      {
        // Ringkøbing
        where: "color = 4",
        polygonOptions: {
          fillColor: "#359CCB",
          fillOpacity: 0.3
        }
      },
      {
        // Ulfborg
        where: "color = 5",
        polygonOptions: {
          fillColor: "#02905D",
          fillOpacity: 0.3
        }
      }
    ]
  });
  layer.setMap(map);
}

events.on("renderItems", function (data) {

  //clear pins first if any
  if (googleMarkers.length !== 0){
    googleMarkers.forEach(function(marker){
      marker.setMap(null);
    })
  }

  var locations = [];
  data.forEach(function (elem) {
    var obj = {};
    obj["lat"] = elem.lat;
    obj["lng"] = elem.lng;
    locations.push(obj);
  });

  var marker;
  var count;

  for (count = 0; count < locations.length; count++) {
    marker = new google.maps.Marker({
      position: locations[count],
      map: googleMap
    });

    googleMarkers.push(marker);

    var product = data[count];
    var contentString = renderContent(product, count);

    google.maps.event.addListener(
      marker,
      "click",
      (function (marker) {
        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });
        return function () {
          infowindow.open(map, marker);
          events.emit('markerClicked', count);
        };
      })(marker)
    );
  }
  if (filter == 0) {
    console.log(filter);
    filterByAll();
  } else if (filter == 1) {
    console.log(filter);
    filterByAllocated();
  } else {
    console.log(filter);
    filterByUnallocated();
  }
  // var markerCluster = new MarkerClusterer(map, marker,
  //     { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' }
  // );
});